document.querySelector('.tasks__all').addEventListener('click', toggleActive);

document.querySelector('.page__menu').addEventListener('click', toggleActive);



const buttons = document.querySelectorAll('.button_default');
buttons.forEach(function(button) {
	button.addEventListener('mousedown', setActive);
	button.addEventListener('mouseup', setInActive);
	button.addEventListener('mouseout', setInActive);
})

const categoryChange = document.querySelector('.tasks__all');
const categoryList = document.querySelector('.all-tasks');

categoryChange.addEventListener('click', function(){
	if (categoryList.classList.contains('is-active')) {
		categoryList.classList.remove('is-active')
	} else {
		categoryList.classList.add('is-active')
	}

})

const tasks = document.querySelectorAll('.task-in-list');
tasks.forEach(function(task) {
	task.addEventListener('click', function(){
		tasks.forEach(function(task){
			task.classList.remove('is-active');
		})
		this.classList.add('is-active');
	})
})



function toggleActive() {
	this.classList.toggle('is-active');
}

function setActive() {
	this.classList.add('is-active');
}
function setInActive() {
	this.classList.remove('is-active');
}
